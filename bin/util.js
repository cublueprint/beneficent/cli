const fs = require("fs");
const { exec, execSync } = require("child_process");

const execCommand = (command, sync = false) => {
  if (sync) {
    execSync(command);
  } else {
    const process = exec(command);
    process.stderr.on("data", (data) => console.error(data));
    process.stdout.on("data", (data) => console.log(data));
  }
};

const writeEnvVars = (envVars) => {
  if (fs.existsSync(`${process.cwd()}/.env`)) {
    fs.unlinkSync(`${process.cwd()}/.env`);
  }
  for (key in envVars) {
    const line = `${key}=${envVars[key]}\n`;
    fs.appendFile(`${process.cwd()}/.env`, line, (err) => {
      if (err) throw err;
    });
  }
};

module.exports = {
  execCommand,
  writeEnvVars
};
